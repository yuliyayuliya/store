package com.julia.store.client;


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.vaadin.polymer.Polymer;
import com.vaadin.polymer.paper.*;

import java.util.Arrays;
/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Store implements EntryPoint {


  public void onModuleLoad() {
      Polymer.startLoading();
      Polymer.importHref(Arrays.asList(
              // We have to load icon sets
              "iron-icons/iron-icons.html",
              // And we have to load all web components in our application
              // before using them as custom elements.
              PaperDrawerPanelElement.SRC,
              PaperHeaderPanelElement.SRC,
              PaperToolbarElement.SRC,
              PaperIconItemElement.SRC,
              PaperRippleElement.SRC,
              PaperIconButtonElement.SRC,
              PaperFabElement.SRC,
              PaperDialogElement.SRC,
              PaperInputElement.SRC,
              PaperTextareaElement.SRC,
              PaperCheckboxElement.SRC,
              PaperButtonElement.SRC), arg -> {
          // The app is executed when all imports succeed.
          startApplication();
          return null;
      });

  }
      private void startApplication() {
          RootPanel.get().add(new Main());
      }

  }
