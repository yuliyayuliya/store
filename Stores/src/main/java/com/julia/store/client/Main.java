package com.julia.store.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;
import com.vaadin.polymer.Polymer;
import com.vaadin.polymer.elemental.HTMLElement;
import com.vaadin.polymer.paper.PaperButtonElement;
import com.vaadin.polymer.paper.PaperDialogElement;
import com.vaadin.polymer.paper.PaperDrawerPanelElement;
import com.vaadin.polymer.paper.PaperFabElement;
import com.vaadin.polymer.paper.PaperIconItemElement;
import com.vaadin.polymer.paper.PaperInputElement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main extends Composite {
    interface MainUiBinder extends UiBinder<HTMLPanel, Main> {
    }

    private static MainUiBinder ourUiBinder = GWT.create(MainUiBinder.class);

    @UiField PaperDrawerPanelElement drawerPanel;

    @UiField PaperIconItemElement menuClearAll;

    @UiField PaperIconItemElement menuClearDone;

    @UiField SuggestBox suggestBox;

    @UiField PaperIconItemElement search;

    @UiField HTMLElement content;

    @UiField PaperFabElement addButton;

    @UiField PaperDialogElement addItemDialog;

    @UiField PaperDialogElement searchDialog;

    @UiField PaperInputElement userName;

    @UiField PaperInputElement priceInput;

    @UiField PaperInputElement dateInput;

    @UiField PaperInputElement countInput;

    @UiField PaperButtonElement confirmAddButton;


    // Our data base is just an array of items in memory
    private List<Item> items = new ArrayList<>();

    public Main() {

        initWidget(ourUiBinder.createAndBindUi(this));
        Polymer.endLoading(this.getElement(), (Element) addButton);

        addButton.addEventListener("tap", e -> addItemDialog.open());

        suggestBox.addValueChangeHandler(e -> {

            String value = e.getValue();

            for (Item item : items) {
                if (item.getUser().equals(value)) {
                    // show selected element
                }
            }
        });

        confirmAddButton.addEventListener("tap", e -> {

            if (!userName.getValue().isEmpty()) {
                addItem(userName.getValue(), dateInput.getValue(), countInput.getValue(), priceInput.getValue());
                // clear text fields
                userName.setValue("");
                priceInput.setValue("");
                dateInput.setValue("");
                countInput.setValue("");
            }
        });

        menuClearAll.addEventListener("tap", e -> {
            closeMenu();
            // remove all child elements
            while (content.hasChildNodes()) {
                content.removeChild(content.getFirstChild());
            }
        });

        menuClearDone.addEventListener("tap", e -> {
            closeMenu();
            for (Item item : items) {
                if (item.isDone()) {
                    content.removeChild(item.getElement());
                    items.remove(item);
                }
            }
        });

        search.addEventListener("tap", e -> {

            closeMenu();
            for (Item item : items) {
                if (item.getUser().equals(suggestBox.getText())) {
                    searchDialog.open();
                }
            }

        });


        suggestBox.addValueChangeHandler(e -> {

            String value = e.getValue();
            for (Item item : items) {
                if (item.getUser().equals(value)) {
                    dateInput.setValue(new Date().toString());
                    priceInput.setValue(item.getProduct());
                    userName.setValue(item.getUser());
                    countInput.setValue(item.getAmount());
                    addItemDialog.open();
                    break;
                }
            }
        });
    }
    private void addItem (String user, String date, String count, String product){
            Item item = new Item();
            item.setUser(user);
            item.setDate(date);
            item.setAmount(count);
            item.setProduct(product);

            MultiWordSuggestOracle oracle = (MultiWordSuggestOracle) suggestBox.getSuggestOracle();
            oracle.add(user);

            content.appendChild(item.getElement());
            items.add(item);
        }

        private void closeMenu () {
            if (drawerPanel.getNarrow()) {
                drawerPanel.closeDrawer();
            }
        }

    }

